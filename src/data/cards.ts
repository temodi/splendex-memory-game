const cardImages =  [
    'assets/angular.png',
    'assets/d3.png',
    'assets/jenkins.png',
    'assets/postcss.png',
    'assets/react.png',
    'assets/redux.png',
    'assets/sass.png',
    'assets/splendex.png',
    'assets/ts.png',
    'assets/webpack.png'
]
 
export default  cardImages;
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { GameComponent } from './game/game.component';
import { RulesComponent } from './rules/rules.component';


const routes: Routes = [
  { path: 'main', component: MainComponent},
  { path: 'game/:deck_size', component: GameComponent}, 
  { path: 'rules', component:  RulesComponent}, 
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: '**', redirectTo: 'main', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router,ActivatedRoute } from  '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {

  postForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router 
  ) {}


  ngOnInit(): void {
    this.postForm = this.formBuilder.group({
        deck_size: [null],
   })
  }
  onSubmit() {
     this.router.navigateByUrl('/game/'+  this.postForm.value.deck_size);
    }
}

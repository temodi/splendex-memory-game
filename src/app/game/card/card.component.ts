import { Component, Input,OnInit } from '@angular/core';
import { CardState } from '../../../models/card-state';

@Component({
  selector: 'game-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
 

export class CardComponent implements OnInit {

  @Input() cardImage: string;
  @Input() cardIndex: number;
  @Input() state;
   
  
  CardStateEnum = CardState;
  /*
  state = CardState.Closed;
*/
  constructor() { }

  ngOnInit(): void {
  }

   
}

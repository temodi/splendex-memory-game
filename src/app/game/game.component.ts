import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from  '@angular/router';
import  CardImages from '../../data/cards'
import { CardState } from '../../models/card-state';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})

export class GameComponent implements OnInit {
  // state$: Observable<object>;
  deckSize: number = 3;
  deck: String[] = [];
  deckStates: CardState[] = [];
  selectedIndexes = {
      first: null,
      second: null
  }
  gameWon: boolean  = false;

  constructor(private activatedRoute: ActivatedRoute) { 
  }

  ngOnInit(): void {
    this.deckSize = parseInt(this.activatedRoute.snapshot.paramMap.get('deck_size'))
    this.guardDeckSize()
    this.startGame()
  }
  guardDeckSize():void {
      if (this.deckSize < 3 && this.deckSize > 10) {
        this.deckSize = 3
      }
  }
  startGame():void {
      this.gameWon = false
      this.fillCards()
  }
  fillCards():void {
    let cards = Array.from(CardImages);
    for(let i  = 0; i < this.deckSize; i++) {
            let index = Math.floor(Math.random() * (cards.length - 1))
            let card = cards[index]
            this.deck.push(card)
            this.deck.push(card)
            cards.splice(index,1)
    }
   
    const fullDeckSize = this.deckSize * 2
    this.deckStates = Array(fullDeckSize).fill(CardState.Closed)
    /*
    for(let i = 0; i < fullDeckSize; i++) {
      this.deckStates.push(CardState.Closed)
    }*/
    this.suffleDeck()
  }

  suffleDeck():void {
    for (let i = this.deck.length - 1; i >= 0; i--) {
      let randomIndex = Math.floor(Math.random() * (i + 1));
      let itemAtIndex = this.deck[randomIndex];
      this.deck[randomIndex] = this.deck[i];
      this.deck[i] = itemAtIndex;
    }
  }
  resetSelectedCards(): void {
      this.deckStates[this.selectedIndexes.first] = CardState.Closed
      this.deckStates[this.selectedIndexes.second] = CardState.Closed
      this.selectedIndexes = {
          first: null,
          second: null
      }
  }
  checkWon():void {
      if (this.deckStates.find(e => e == CardState.Closed) == undefined) {
          this.gameWon = true
      }
  }
  checkMatch():void {
    if (this.selectedIndexes.first != null && this.selectedIndexes.second != null ) {
          if (this.deck[this.selectedIndexes.first] == this.deck[this.selectedIndexes.second] ) {
              this.selectedIndexes = {
                  first: null,
                  second: null
              }
              this.checkWon()
          } else {
             setTimeout(() => this.resetSelectedCards(), 200);
          }
    }
  }

  handleClick(index: number):void  {
    if (this.selectedIndexes.first != null && this.selectedIndexes.second != null ) 
        return

     if (this.deckStates[index] == CardState.Closed) {
          this.deckStates[index] = CardState.Opened
          if (this.selectedIndexes.first == null) {
            this.selectedIndexes.first = index
          } else {
            this.selectedIndexes.second = index
          }
     }
    this.checkMatch()
 }

}

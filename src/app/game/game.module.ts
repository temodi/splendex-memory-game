import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './game.component';
import { CardComponent } from './card/card.component';
 
@NgModule({
  declarations: [GameComponent, CardComponent],
  imports: [
    CommonModule,
  ]
})
export class GameModule { }
